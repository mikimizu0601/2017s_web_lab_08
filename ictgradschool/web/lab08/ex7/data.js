/**
 * Created by mmiz318 on 7/12/2017.
 */
function fillDetails(i){
    var name = customers[i].name;
    var gender = customers[i].gender;
    var birth = customers[i].year_born;
    var join = customers[i].joined;
    var hires = customers[i].num_hires;
    var row = document.createElement("tr");

    var cell = document.createElement("td");
    row.appendChild(cell);
    cell.innerHTML= name;
    document.getElementById("details").appendChild(row);

    var cell = document.createElement("td");
    row.appendChild(cell);
    cell.innerHTML= gender;
    document.getElementById("details").appendChild(row);

    var cell = document.createElement("td");
    row.appendChild(cell);
    cell.innerHTML= birth;
    document.getElementById("details").appendChild(row);

    var cell = document.createElement("td");
    row.appendChild(cell);
    cell.innerHTML= join;
    document.getElementById("details").appendChild(row);

    var cell = document.createElement("td");
    row.appendChild(cell);
    cell.innerHTML= hires;
    document.getElementById("details").appendChild(row);
}
var female = 0;
var male = 0;
var ageOne = 0;
var ageTwo = 0;
var ageThree = 0;
var gold = 0;
var silver = 0;
var bronze = 0;

function fillStats() {
    var row = document.createElement("tr");
    var cell = document.createElement("td");
    row.appendChild(cell);
    cell.innerHTML= female;
    document.getElementById("stats").appendChild(row);

    var cell = document.createElement("td");
    row.appendChild(cell);
    cell.innerHTML= male;
    document.getElementById("stats").appendChild(row);

    var cell = document.createElement("td");
    row.appendChild(cell);
    cell.innerHTML= ageOne;
    document.getElementById("stats").appendChild(row);

    var cell = document.createElement("td");
    row.appendChild(cell);
    cell.innerHTML= ageTwo;
    document.getElementById("stats").appendChild(row);

    var cell = document.createElement("td");
    row.appendChild(cell);
    cell.innerHTML= ageThree;
    document.getElementById("stats").appendChild(row);

    var cell = document.createElement("td");
    row.appendChild(cell);
    cell.innerHTML= gold;
    document.getElementById("stats").appendChild(row);

    var cell = document.createElement("td");
    row.appendChild(cell);
    cell.innerHTML= silver;
    document.getElementById("stats").appendChild(row);

    var cell = document.createElement("td");
    row.appendChild(cell);
    cell.innerHTML= bronze;
    document.getElementById("stats").appendChild(row);
}


for(var i = 0; i < customers.length; i++){
    // console.log(customers);
    fillDetails(i);

    if(customers[i].gender == "female"){
        female++;
    } else {
        male++;
    }
    if(customers[i].year_born >= 1987){
        ageOne++;
    } else if(customers[i].year_born <= 1952){
        ageThree++;
    } else {
        ageTwo++;
    }
    var avg = (customers[i].num_hires / (2017 - customers[i].joined)) / 52 ;
    if(avg > 4 ){
        gold++;
    } else if(avg < 1){
        bronze++;
    } else {
        silver++;
    }

}
fillStats();